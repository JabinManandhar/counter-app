import React, { Component } from 'react';
import "../src/App.css";
import Button from './components/Button'

export default class App extends Component {
  state = {
    count: 0,
    showErrorMessage: false,
    message: ""
  }


  //For resetting the counter to zero.
  handleReset = () => {
    this.setState(
      {
        count: 0,
        showErrorMessage: false,
        message: ""
      }
    )
  }


  //Using ES6
  handleIncrement = () => {
    this.setState({
      count: this.state.count + 1,
      showErrorMessage: false,
      message: ""
    })
  }
  handleDecrement = () => {
    if (this.state.count > 0) {
      this.setState({
        count: this.state.count - 1
      })
    }
    else {
      this.setState({
        showErrorMessage: true,
        message: "Count cannot be less than 0"
      })
    }
  }

  // handleCount(type) {
  //   if (type === 'increment') {
  //     this.setState({
  //       count:this.state.count+1
  //     })
  //     else if(type === 'decrement') {

  //     this.setState({
  //       count:this.state.count-1
  //     })
  //     }
  //   }
  // }
  render() {
    return (
      <div id="root">
        <div className="container">
          <h1>Counter App</h1>
          <h3>Count:{this.state.count}</h3>
          {/* <button onClick={() => { this.handleIncrement() }}>Increment</button>
          <button onClick={() => { this.handleDecrement() }}>Decrement</button>
          <button onClick={() => { this.handleReset() }}>Reset</button> */}

          <Button
            flag="incrementCount"
            title="Increment"
            task={() => { this.handleIncrement() }}
          />
          <Button
            flag="decrementCount"
            title="Decrement"
            task={() => { this.handleDecrement() }}
          />
          <Button
            flag="resetCount"
            title="Reset"
            task={() => { this.handleReset() }}
          />
            {this.state.showErrorMessage === true ?
              <p style={{ color: "red" }}>{this.state.message}</p>
              : ""
          }
        </div>
      </div>
    )
  }
}

